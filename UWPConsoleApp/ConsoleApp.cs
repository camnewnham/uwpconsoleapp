﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWPConsoleApp
{
    class ConsoleApp
    {
        public static void MainProgram(params string[] args)
        {
            Console.WriteLine("A test!");
            Console.ReadKey();
            Console.WriteLine("A key!");
            Console.ReadKey();
            Console.Exit();
        }
    }
}
