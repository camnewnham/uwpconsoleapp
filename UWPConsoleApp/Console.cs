using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

namespace UWPConsoleApp
{
    public static class Console
    {
        private static MainPage page;
        static ManualResetEvent mre;

        public static void Initialize(MainPage source)
        {
            page = source;
            CoreWindow.GetForCurrentThread().KeyDown += Console_KeyDown;
            mre = new ManualResetEvent(false);
        }

        private static void Console_KeyDown(CoreWindow sender, KeyEventArgs args)
        {
            mre.Set();
        }

        public static void ReadKey()
        {
            // start a thread that waits for the reset event
            mre.Reset();
            mre.WaitOne();
        }

        public static void WriteLine(string text)
        {
            DoLog(text);
        }

        private static async void DoLog(string text)
        {
            await page.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                page.ConsoleWindow.Text += text + Environment.NewLine;
            });
        }

        public static void Exit()
        {
            Windows.ApplicationModel.Core.CoreApplication.Exit();
        }

    }
}
