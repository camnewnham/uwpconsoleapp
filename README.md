# README #

This is a very simple console application using UWP. 
It is intended to emulate a .NET console application for the purposes of testing cross-platform libraries.

Edit the code in ConsoleApp.cs.

### Supported Features ###
* Console.WriteLine()
* Console.ReadKey()
* Console.Exit()

### Example ###

```
public static void MainProgram(params string[] args)
{
	Console.WriteLine("A test!");
	Console.ReadKey();
	Console.WriteLine("A key!");
	Console.ReadKey();
	Console.Exit();
}
```

![Console Image](https://bytebucket.org/camnewnham/uwpconsoleapp/raw/262e40860c1eee41c951b0e24b8de0dd7974f46f/UWPConsoleApp/Assets/uwp_console_app.png)